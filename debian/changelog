python-senlinclient (3.1.0-4) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090615).

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 Dec 2024 22:40:38 +0100

python-senlinclient (3.1.0-3) unstable; urgency=medium

  * Removed /usr/bin alternative removal in {pre,post}rm.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Sep 2024 10:35:27 +0200

python-senlinclient (3.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2023 17:00:05 +0200

python-senlinclient (3.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 31 Aug 2023 11:18:30 +0200

python-senlinclient (3.0.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1049187).

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Aug 2023 14:25:23 +0200

python-senlinclient (3.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 13:23:50 +0200

python-senlinclient (3.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Feb 2023 14:20:25 +0100

python-senlinclient (2.5.0-3) unstable; urgency=medium

  * Removed python3-mox3 from build-depends.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Dec 2022 18:33:44 +0100

python-senlinclient (2.5.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 14:42:35 +0200

python-senlinclient (2.5.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 31 Aug 2022 13:31:26 +0200

python-senlinclient (2.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 11:38:30 +0100

python-senlinclient (2.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Feb 2022 21:58:55 +0100

python-senlinclient (2.3.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 17:53:01 +0200

python-senlinclient (2.3.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Sep 2021 09:36:52 +0200

python-senlinclient (2.2.1-1) experimental; urgency=medium

  * New upstream release.
  * Removed build-depends version of openstack-pkg-tools in advance of
    Bullseye release.
  * Bump standards-version to 4.5.1.

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Mar 2021 16:21:03 +0100

python-senlinclient (2.1.1-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2020 17:20:09 +0200

python-senlinclient (2.1.1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Switch to debhelper-compat 11.

 -- Thomas Goirand <zigo@debian.org>  Thu, 17 Sep 2020 08:40:19 +0200

python-senlinclient (2.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 10 Sep 2020 12:20:10 +0200

python-senlinclient (2.0.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 23:38:12 +0200

python-senlinclient (2.0.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 08 Apr 2020 16:59:28 +0200

python-senlinclient (1.11.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 01:49:07 +0200

python-senlinclient (1.11.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 23 Sep 2019 17:37:35 +0200

python-senlinclient (1.10.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Jul 2019 14:31:55 +0200

python-senlinclient (1.10.1-1) experimental; urgency=medium

  * New upstream release.
  * Removed Python 2 support.

 -- Thomas Goirand <zigo@debian.org>  Sat, 23 Mar 2019 22:45:58 +0100

python-senlinclient (1.8.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 04 Sep 2018 22:43:38 +0200

python-senlinclient (1.8.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Bumped Standards-Version: to 4.2.0 (no change).
  * Building doc with Python 3.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Aug 2018 13:45:22 +0200

python-senlinclient (1.7.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 21:56:07 +0000

python-senlinclient (1.7.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * Running wrap-and-sort -bast

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed .postinst, since /usr/bin/senlin doesn't exist anymore.

 -- Thomas Goirand <zigo@debian.org>  Tue, 13 Feb 2018 21:49:07 +0000

python-senlinclient (1.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Nov 2017 00:40:47 +0000

python-senlinclient (1.4.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Removed UPSTREAM_GIT with default value
  * d/copyright: Changed source URL to https protocol

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using pkgos-dh_auto_{install,test}.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2017 11:55:16 +0200

python-senlinclient (0.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 16:48:25 +0000

python-senlinclient (0.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Mar 2016 13:46:26 +0100

python-senlinclient (0.3.0-1) experimental; urgency=medium

  [ David Della Vecchia ]
  * New upstream release.
  * d/control: Align requirements with upstream, add git for sphinx.

  [ Corey Bryant ]
  * d/control: Update uploaders.

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).

  [ Thomas Goirand ]
  * Standards-Version: 3.9.7.

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 Mar 2016 08:14:02 +0000

python-senlinclient (0.2.1-1) experimental; urgency=medium

  [ David Della Vecchia ]
  * New upstream release.
  * d/control: Align requirements with upstream.

  [ Thomas Goirand ]
  * Fixed debian/copyright ordering.

 -- Thomas Goirand <zigo@debian.org>  Mon, 18 Jan 2016 12:09:42 +0000

python-senlinclient (0.1.8-1) unstable; urgency=medium

  * Initial release. (Closes: #807405)

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Dec 2015 14:28:25 +0100
